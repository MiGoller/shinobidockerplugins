#!/bin/bash

set -e

apk add --update --no-cache nodejs git
git clone https://gitlab.com/Shinobi-Systems/Shinobi.git ./ShinobiPro

export APP_VERSION=$( node -pe "require('./ShinobiPro/package.json')['version']" )

chmod +x *.sh
