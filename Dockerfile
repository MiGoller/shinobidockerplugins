#
# Docker image template for Shinobi Plugins
#
FROM node:8

# Build arguments ...
# Plugin's version information
ARG ARG_APP_VERSION 

# The channel or branch triggering the build.
ARG ARG_APP_CHANNEL

# The commit sha triggering the build.
ARG ARG_APP_COMMIT

# Build data
ARG ARG_BUILD_DATE

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.build-date=${ARG_BUILD_DATE} \
    org.label-schema.docker.dockerfile="/Dockerfile" \
    org.label-schema.license="GPLv3" \
    org.label-schema.name="MiGoller" \
    org.label-schema.vendor="MiGoller" \
    org.label-schema.version=${ARG_APP_VERSION} \
    org.label-schema.description="Shinobi ${ARG_FLAVOR} plugin" \
    org.label-schema.url="https://gitlab.com/users/MiGoller/projects" \
    org.label-schema.vcs-ref=${ARG_APP_COMMIT} \
    org.label-schema.vcs-type="Git" \
    org.label-schema.vcs-url="https://gitlab.com/MiGoller/shinobidockerplugins.git" \
    maintainer="MiGoller" \
    Author="MiGoller"

# Persist app-reladted build arguments
ENV APP_VERSION=$ARG_APP_VERSION \
    APP_CHANNEL=$ARG_APP_CHANNEL \
    APP_COMMIT=$ARG_APP_COMMIT \
    APP_UPDATE=$ARG_APP_UPDATE

# Set environment variables to default values
#   TODO:   Modify this section to set the configuration for the plugin!
#           Have a look at the configuration for the Motion plugin below!
#{
#  "plug":"Motion",
#  "host":"localhost",
#  "port":8080,
#  "key":"change_this_to_something_very_random____make_sure_to_match__/plugins/motion/conf.json",
#  "notice":"Looks like you have the Motion plugin running. Don't forget to enable <b>Send Frames</b> to start pushing frames to be read."
#}

ENV PLUGIN_KEY="b7502fd9-506c-4dda-9b56-8e699a6bc41c" \
    PLUGIN_HOST="localhost" \
    PLUGIN_PORT="8080"

# Create additional directories for: Custom configuration, working directory, database directory, scripts
RUN mkdir -p \
        /config \
        /opt/plugin

# Assign working directory
WORKDIR /opt/plugin

RUN echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list

# Install plugin dependencies
RUN apt-get update && \
    apt-get install -y \
        build-essential \
        g++ \
        libcairo2-dev \
        libgif-dev \
        libjpeg-dev \
        libpango1.0-dev \
        make \
        pkg-config \
        python
        
# Copy the plugin's sources
#   TODO:   Modify this section to set the configuration for the plugin!
#           Have a look at the configuration for the Motion plugin below!
COPY /ShinobiPro/package.json /ShinobiPro/plugins/motion ./

# Install the plugin
RUN npm i npm@latest -g && \
    npm install pm2 -g && \
    npm install canvas --unsafe-perm && \
    npm install --unsafe-perm && \
    npm audit fix --force

# Copy code
COPY /docker-entrypoint.sh ./
RUN chmod -f +x ./*.sh

# Copy PM2 config
COPY /pm2ShinobiPlugin.yml ./

VOLUME ["/config"]

EXPOSE 8080

ENTRYPOINT ["/opt/plugin/docker-entrypoint.sh"]

CMD ["pm2-docker", "pm2ShinobiPlugin.yml"]
